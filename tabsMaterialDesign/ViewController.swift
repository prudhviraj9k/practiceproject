//
//  ViewController.swift
//  tabsMaterialDesign
//
//  Created by prudhvi on 11/09/19.
//  Copyright © 2019 prudhvi. All rights reserved.
//

import UIKit
import MaterialComponents.MaterialTabs

class ViewController: UIViewController,MDCTabBarDelegate {
    var tabBar = MDCTabBar()
    @IBOutlet weak var nameLbl: UILabel!
  
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tabBar = MDCTabBar(frame: view.bounds)
        tabBar.delegate = self
        tabBar.items = [
            UITabBarItem(title: "Screen 1", image: UIImage(named: ""), tag: 0),
            UITabBarItem(title: "Screen 2", image: UIImage(named: ""), tag: 0),UITabBarItem(title: "Screen 3", image: UIImage(named: ""), tag: 0),
            UITabBarItem(title: "Screen 4", image: UIImage(named: ""), tag: 0)]
        tabBar.barTintColor = #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1)
        tabBar.itemAppearance = .titledImages
        tabBar.autoresizingMask = [.flexibleWidth, .flexibleBottomMargin]
        tabBar.sizeToFit()
        view.addSubview(tabBar)
        addTabbar()
        let screenOneVC = self.storyboard?.instantiateViewController(withIdentifier: "ScreenOneViewController") as! ScreenOneViewController
        screenOneVC.stringOne = "Screen One"
        self.view.addSubview(screenOneVC.view)
    }
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        addHeightConstraintToTabbar()
    }
    func addTabbar() {
        tabBar.translatesAutoresizingMaskIntoConstraints = false
        tabBar.rightAnchor.constraint(equalTo: self.view.rightAnchor).isActive = true
        tabBar.leftAnchor.constraint(equalTo: self.view.leftAnchor).isActive = true
        tabBar.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
    }
    func addHeightConstraintToTabbar() {
        let heightConstant:CGFloat = self.view.safeAreaInsets.top + 44.0
        tabBar.heightAnchor.constraint(equalToConstant: heightConstant).isActive = true
    }
    func tabBar(_ tabBar: MDCTabBar, didSelect item: UITabBarItem) {
        if(item == tabBar.items[0]){
            let screenOneVC = self.storyboard?.instantiateViewController(withIdentifier: "ScreenOneViewController") as! ScreenOneViewController
            screenOneVC.stringOne = "Screen One"
            print("selcted 0")
            self.view.addSubview(screenOneVC.view)
            //present(screenOneVC,animated: true)
        }
        else if(item == tabBar.items[1]){
            let screenTwoVC = self.storyboard?.instantiateViewController(withIdentifier: "ScreenTwoViewController") as! ScreenTwoViewController
            self.view.addSubview(screenTwoVC.view)
        }
        else if(item == tabBar.items[2]){
            let screenThreeVC = self.storyboard?.instantiateViewController(withIdentifier: "ScreenThreeViewController") as! ScreenThreeViewController
            self.view.addSubview(screenThreeVC.view)
        }
        else{
            let screenFourVC = self.storyboard?.instantiateViewController(withIdentifier: "ScreenFourViewController") as! ScreenFourViewController
            self.view.addSubview(screenFourVC.view)
        }
        
    }
}

